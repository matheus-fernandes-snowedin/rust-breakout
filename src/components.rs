use bevy::prelude::{Component, Vec2, Entity};

#[derive(Component)]
pub struct PointsText;

#[derive(Component)]
pub struct Velocity {
    pub x: f32,
    pub y: f32
}

#[derive(Component)]
pub struct Brick {
	pub points: f32
}

#[derive(Component)]
pub struct Ball {
	started: bool
}

#[derive(Component)]
pub struct Paddle;

#[derive(Component)]
pub struct Movable;

#[derive(Component)]
pub struct MenuComponent;

#[derive(Component)]
pub struct BreakoutButton {
	pub hover: bool,
	pub text: String,
	pub label_offset: Vec2,
	pub text_id: Entity,
	pub callback: fn(),
}

#[derive(Component)]
pub struct SpriteSize(pub Vec2);

impl From<(f32, f32)> for SpriteSize {
	fn from(val: (f32, f32)) -> Self {
		SpriteSize(Vec2::new(val.0, val.1))
	}
}

impl Ball {
	pub fn set_started (&mut self, value:bool) {
		self.started = value;
	}

	pub fn has_started (&self) -> bool {
		self.started
	}
}

impl Default for Ball {
	fn default() -> Self {
		Ball {
			started: false
		}
	}
}