use bevy::{prelude::*, input::mouse::MouseMotion};

use crate::{GameTextures, components::{BreakoutButton, SpriteSize, MenuComponent}, BUTTON_SIZE_SPRITE, WinSize};


pub struct ButtonPlugin;

impl Plugin for ButtonPlugin {
	fn build(&self, app: &mut App) {
		app
        // .add_startup_system_to_stage(StartupStage::PostStartup, testing_button)
        .add_system(button_system);
	}
}

impl ButtonPlugin {
    pub fn add_button(mut commands: Commands, win_size: Res<WinSize>, game_resources: Res<GameTextures>, position: Vec3, label_offset: Vec2, text: &str, callback: fn()) {
        let mut text_entity_commands = commands.spawn(TextBundle::from_section(
            text,
            TextStyle {
                font: game_resources.hud_text_font.clone(),
                font_size: 24.0,
                color: Color::rgb(0.1, 0.1, 0.1),
            },
        )
        .with_text_alignment(TextAlignment::CENTER)
        .with_style(Style {
			align_self: AlignSelf::Center,
            position_type: PositionType::Absolute,
            position: UiRect {
				top: Val::Px(-position.y + win_size.h / 2.0 + label_offset.y - BUTTON_SIZE_SPRITE.1 / 2.0 + 12.),
				left: Val::Px(position.x + win_size.w / 2.0 + label_offset.x),
                ..default()
            },
            ..default()
        }));

        text_entity_commands.insert(MenuComponent);
        let text_entity = text_entity_commands.id();

        commands.spawn(SpriteBundle {
            texture: game_resources.button_normal.clone(),
            transform: Transform {
                translation: position,
                ..default()
            },
            ..default()
        })
        .insert(MenuComponent)
        .insert(BreakoutButton{ hover: false, text: text.to_string(), label_offset, text_id: text_entity, callback })
        .insert(SpriteSize::from(BUTTON_SIZE_SPRITE));

        
    }
}

fn button_test_callback() {
    println!("TEST!");
}

fn testing_button(mut commands: Commands, win_size: Res<WinSize>, game_resources: Res<GameTextures>) {
    ButtonPlugin::add_button(commands, win_size, game_resources, Vec3 { x: 0., y: 0., z:0. }, Vec2 {x: -40., y: 0. }, "Testing!", button_test_callback);
}

fn button_system(mut commands: Commands, windows: Res<Windows>, win_size: Res<WinSize>, buttons: Res<Input<MouseButton>>,game_resources: Res<GameTextures>, mut query: Query<(Entity, &Transform, &SpriteSize, &mut BreakoutButton), With<BreakoutButton>>) {
    let window = windows.get_primary().unwrap();
    let cursor_position_option = window.cursor_position();
    if cursor_position_option.is_none() { return; }
    let mut cursor_position:Vec2 = cursor_position_option.unwrap();
    cursor_position.x = cursor_position.x - win_size.w/2.;
    cursor_position.y = cursor_position.y - win_size.h/2.;

    for (entity, transform, size, mut button) in query.iter_mut() {
        let mut should_state_hover = true;
        if (cursor_position.x < transform.translation.x - size.0.x/2. ||
            cursor_position.x > transform.translation.x + size.0.x/2. ||
            cursor_position.y < transform.translation.y - size.0.y/2. ||
            cursor_position.y > transform.translation.y + size.0.y/2.) {
                // mouse out and button is already normal, skip it
                if !button.hover { continue; }
                // mouse is out and we want to change the state to normal again
                should_state_hover = false;
        }

        if buttons.just_pressed(MouseButton::Left) && should_state_hover {
            (button.callback)();
        }

        if should_state_hover && button.hover { continue; }

        commands.entity(button.text_id).despawn();
        
        
        let texture = if should_state_hover { game_resources.button_hover.clone() } else { game_resources.button_normal.clone() };
        let mut text_entity_commands = commands.spawn(TextBundle::from_section(
            &button.text,
                TextStyle {
                    font: game_resources.hud_text_font.clone(),
                    font_size: 24.0,
                    color: Color::rgb(0.1, 0.1, 0.1),
                },
            )
            .with_text_alignment(TextAlignment::CENTER)
            .with_style(Style {
                align_self: AlignSelf::Center,
                position_type: PositionType::Absolute,
                position: UiRect {
                    top: Val::Px(-transform.translation.y + win_size.h / 2.0 + button.label_offset.y - BUTTON_SIZE_SPRITE.1/2.0 + 12.),
                    left: Val::Px(transform.translation.x + win_size.w / 2.0 + button.label_offset.x),
                    ..default()
                },
                ..default()
            }));

            text_entity_commands.insert(MenuComponent);
            let text_id = text_entity_commands.id();

        commands.spawn(SpriteBundle {
                texture,
                transform: Transform {
                    translation: transform.translation,
                    ..default()
                },
                ..default()
            })
            .insert(MenuComponent)
            .insert(BreakoutButton { hover: should_state_hover, text: button.text.to_string(), label_offset: button.label_offset, text_id: text_id, callback: button.callback })
            .insert(SpriteSize::from(BUTTON_SIZE_SPRITE));

        commands.entity(entity).despawn();
    }
}