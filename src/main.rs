#![allow(unused)] use std::env;

// silence unused warnings while exploring (to comment out)
use bevy::prelude::*;

use button::ButtonPlugin;
use components::{Velocity, Movable, Brick, Ball, Paddle, MenuComponent};
use paddle::PaddlePlugin;
use brick::BrickPlugin;
use ball::BallPlugin;
use hud::HUDPlugin;

mod components;
mod button;
mod paddle;
mod brick;
mod ball;
mod hud;

// region:    --- Asset Constants

const HUD_POINTS_TEXT_FONT_PATH: &str = "fonts/FiraMono-Medium.ttf";
const HUD_TEXT_FONT_PATH: &str = "fonts/FiraSans-Bold.ttf";

const PADDLE_SIZE_SPRITE: (f32, f32) = (104., 24.);
const BUTTON_SIZE_SPRITE: (f32, f32) = (190., 49.);
const BRICK_SIZE_SPRITE: (f32, f32) = (64., 32.);
const BALL_SIZE_SPRITE: (f32, f32) = (22., 22.);

const BRICK_YELLOW_SPRITE: &str = "brick_yellow.png";
const BRICK_PURPLE_SPRITE: &str = "brick_purple.png";
const BRICK_GREEN_SPRITE: &str = "brick_green.png";
const BRICK_GREY_SPRITE: &str = "brick_grey.png";
const BRICK_BLUE_SPRITE: &str = "brick_blue.png";
const BRICK_RED_SPRITE: &str = "brick_red.png";
const PADDLE_SPRITE: &str = "paddle.png";
const BALL_SPRITE: &str = "ball.png";

const BUTTON_NORMAL_SPRITE: &str = "button_default.png";
const BUTTON_HOVER_SPRITE: &str = "button_selected.png";

// endregion: --- Asset Constants

// region:    --- Game Constants

const TIME_STEP: f32 = 1. / 60.;
const BASE_SPEED: f32 = 500.;

static mut SHOULD_RESTART:bool = false;

// endregion: --- Game Constants

#[derive(Resource)]
pub struct WinSize {
	pub w: f32,
	pub h: f32,
}

#[derive(Resource)]
pub struct GameTextures {
	hud_points_text_font: Handle<Font>,
	hud_text_font: Handle<Font>,

	paddle: Handle<Image>,
	ball: Handle<Image>,
	
    brick_yellow: Handle<Image>,
    brick_green: Handle<Image>,
    brick_grey: Handle<Image>,
    brick_purple: Handle<Image>,
    brick_blue: Handle<Image>,
	brick_red: Handle<Image>,

	button_normal: Handle<Image>,
	button_hover: Handle<Image>,
}

#[derive(PartialEq, Clone, Copy, Debug)]
enum GameState {
	Menu,
	Pause,
	WaitToStart,
	InGame,
	Restart,
	Victory,
	GameOver	
}

#[derive(Resource)]
struct PlayerState {
	points: f32,
	last_state: GameState,
	state: GameState
}

fn main() {
	env::set_var("RUST_BACKTRACE", "full");
    App::new()
		.insert_resource(ClearColor(Color::rgb(0.04, 0.04, 0.04)))
		.insert_resource(PlayerState { 
			points: 0.,
			state: GameState::WaitToStart,
			last_state: GameState::WaitToStart
		})
		.add_plugins(DefaultPlugins.set(WindowPlugin {
			window: WindowDescriptor {
				title: "Breakout".to_string(),
				width: 598.0,
				height: 676.0,
				..Default::default()
			},
			..Default::default()
		}))
		.add_plugin(PaddlePlugin)
		.add_plugin(BallPlugin)
		.add_plugin(BrickPlugin)
		.add_plugin(HUDPlugin)
		.add_plugin(ButtonPlugin)
		.add_startup_system(setup_system)
		.add_system(movable_system)
		.add_system(game_state_update)
		.run();
}

fn setup_system(mut commands:Commands, asset_server: Res<AssetServer>, windows: Res<Windows>) {
	commands.spawn(Camera2dBundle::default());

	let window = windows.get_primary().unwrap();
	let (win_w, win_h) = (window.width(), window.height());
	let win_size = WinSize { w: win_w, h: win_h };
	commands.insert_resource(win_size);

	let game_textures = GameTextures {
		hud_points_text_font: asset_server.load(HUD_POINTS_TEXT_FONT_PATH),
		hud_text_font: asset_server.load(HUD_TEXT_FONT_PATH),

		paddle:  asset_server.load(PADDLE_SPRITE),
		ball: asset_server.load(BALL_SPRITE),
		
		brick_yellow: asset_server.load(BRICK_YELLOW_SPRITE),
		brick_purple: asset_server.load(BRICK_PURPLE_SPRITE),
		brick_green: asset_server.load(BRICK_GREEN_SPRITE),
		brick_grey: asset_server.load(BRICK_GREY_SPRITE),
		brick_blue: asset_server.load(BRICK_BLUE_SPRITE),
		brick_red: asset_server.load(BRICK_RED_SPRITE),

		button_normal: asset_server.load(BUTTON_NORMAL_SPRITE),
		button_hover: asset_server.load(BUTTON_HOVER_SPRITE),
	};
	commands.insert_resource(game_textures);
}

fn movable_system(
	mut commands: Commands,
	mut query: Query<(Entity, &Velocity, &mut Transform, &Movable)>,
) {
	for (entity, velocity, mut transform, movable) in query.iter_mut() {
		let translation = &mut transform.translation;
		translation.x += velocity.x * TIME_STEP * BASE_SPEED;
		translation.y += velocity.y * TIME_STEP * BASE_SPEED;
	}
}

fn game_state_update(mut commands: Commands, 
	game_resources: Res<GameTextures>,
	win_size: Res<WinSize>,
	mut game_state: ResMut<PlayerState>,
	query_ball: Query<Entity, With<Ball>>,
	query_brick: Query<Entity, With<Brick>>,
	query_gameover: Query<Entity, With<MenuComponent>>,
	mut paddle_query: Query<&mut Transform, With<Paddle>>,
) {
	unsafe {
		if SHOULD_RESTART {
			SHOULD_RESTART = false;
			game_state.state = GameState::Restart;
		}
	}

	if game_state.last_state == game_state.state {
		return;
	}

	game_state.last_state = game_state.state;
	match game_state.state {
		GameState::Menu => todo!(),
		GameState::Pause => todo!(),
		GameState::InGame => {},
		GameState::WaitToStart => {},
		GameState::Restart => {
			game_state.state = GameState::WaitToStart;
			restart_game(&mut commands, game_state, win_size, game_resources, paddle_query, query_gameover);
		},
		GameState::Victory => victory(commands, game_resources, win_size, query_ball, query_brick),
		GameState::GameOver => game_over(commands, game_resources, win_size, query_ball, query_brick)
	}
}

fn restart_game(mut commands: &mut Commands,
	mut game_state: ResMut<PlayerState>,
	win_size: Res<WinSize>,
	game_resources: Res<GameTextures>,
	mut query: Query<&mut Transform, With<Paddle>>,
	query_menucomponent: Query<Entity, With<MenuComponent>>,
) {
	for menucomponent_entity in query_menucomponent.iter() {
		commands.entity(menucomponent_entity).despawn();
	}

	BallPlugin::spawn_ball(&mut commands, &win_size, &game_resources);
	BrickPlugin::brick_spawn(commands, game_resources, win_size);
	game_state.points = 0.;

	if let Ok(mut transform) = query.get_single_mut() {
		transform.translation.x = 0.;
	}

	println!(">>> Restarting Game...");
}

fn game_over(mut commands: Commands,
	game_resources: Res<GameTextures>,
	win_size: Res<WinSize>,
	query_ball: Query<Entity, With<Ball>>,
	query_brick: Query<Entity, With<Brick>>
) {
	for ball_entity in query_ball.iter() {
		commands.entity(ball_entity).despawn();
	}

	for brick_entity in query_brick.iter() {
		commands.entity(brick_entity).despawn();
	}

	let text_size_w = 220.;
	let text_size_h = 100.;
	commands.spawn((
        TextBundle::from_section(
            "Game Over!",
            TextStyle {
                font: game_resources.hud_text_font.clone(),
                font_size: 100.0,
                color: Color::WHITE,
            },
        )
        .with_text_alignment(TextAlignment::CENTER)
        .with_style(Style {
			align_self: AlignSelf::Center,
            position_type: PositionType::Absolute,
            position: UiRect {
				top: Val::Px(win_size.h / 2. - text_size_h),
				left: Val::Px(win_size.w / 2. - text_size_w),
                ..default()
            },
            ..default()
        }),
		MenuComponent
    ));
	ButtonPlugin::add_button(commands, win_size, game_resources, Vec3::new(0.,-40.,0.), Vec2::new(-44.,0.), "RESTART", game_state_restart);
	println!(">>> Game Over...");
}

fn victory(mut commands: Commands,
	game_resources: Res<GameTextures>,
	win_size: Res<WinSize>,
	query_ball: Query<Entity, With<Ball>>,
	query_brick: Query<Entity, With<Brick>>
) {
	for ball_entity in query_ball.iter() {
		commands.entity(ball_entity).despawn();
	}

	let text_size_w = 140.;
	let text_size_h = 100.;
	commands.spawn((
        TextBundle::from_section(
            "Victory!",
            TextStyle {
                font: game_resources.hud_text_font.clone(),
                font_size: 100.0,
                color: Color::WHITE,
            },
        )
        .with_text_alignment(TextAlignment::CENTER)
        .with_style(Style {
			align_self: AlignSelf::Center,
            position_type: PositionType::Absolute,
            position: UiRect {
				top: Val::Px(win_size.h / 2. - text_size_h),
				left: Val::Px(win_size.w / 2. - text_size_w),
                ..default()
            },
            ..default()
        }),
		MenuComponent
    ));
	ButtonPlugin::add_button(commands, win_size, game_resources, Vec3::new(0.,-40.,0.), Vec2::new(-44.,0.), "RESTART", game_state_restart);
	println!(">>> Victory...");
}

fn game_state_restart() {
	unsafe {
		SHOULD_RESTART = true;
	}
}