use bevy::prelude::*;

use crate::{GameTextures, components::{Brick, SpriteSize}, BRICK_SIZE_SPRITE, WinSize};

pub struct BrickPlugin;

impl Plugin for BrickPlugin {
	fn build(&self, app: &mut App) {
		app
        .add_startup_system_to_stage(StartupStage::PostStartup, BrickPlugin::internal_brick_spawn);
	}
}

impl BrickPlugin {
    fn internal_brick_spawn(mut commands: Commands, game_resources: Res<GameTextures>, win_size: Res<WinSize>) {    
        BrickPlugin::brick_spawn(&mut commands, game_resources, win_size);
    }

    pub fn brick_spawn(mut commands: &mut Commands, game_resources: Res<GameTextures>, win_size: Res<WinSize>) {    
        let brick_vertical_space = BRICK_SIZE_SPRITE.1 + 5.;
        let brick_horizontal_space = BRICK_SIZE_SPRITE.0 + 5.;
    
        let n_bricks_horizontal:i32 = (win_size.w / brick_horizontal_space).floor() as i32;
        let total_bricks_length= (win_size.w - (n_bricks_horizontal as f32) * brick_horizontal_space);
        let mut x: f32 = -win_size.w / 2. + BRICK_SIZE_SPRITE.0 / 2. + total_bricks_length / 2.;
        let mut y: f32 = 0.;
        for n in 0..n_bricks_horizontal {
            brick_spawn_at(&mut commands, game_resources.brick_blue.clone(), Vec3::new(x, y, 0.), 15.);
    
            let green_vertical_position = y + brick_vertical_space;
            brick_spawn_at(&mut commands, game_resources.brick_green.clone(), Vec3::new(x, green_vertical_position, 0.), 25.);
    
            let grey_vertical_position = green_vertical_position + brick_vertical_space;
            brick_spawn_at(&mut commands, game_resources.brick_grey.clone(), Vec3::new(x, grey_vertical_position, 0.), 42.);
    
            let yellow_vertical_position = grey_vertical_position + brick_vertical_space;
            brick_spawn_at(&mut commands, game_resources.brick_yellow.clone(), Vec3::new(x, yellow_vertical_position, 0.), 50.);
    
            let purple_vertical_position = yellow_vertical_position + brick_vertical_space;
            brick_spawn_at(&mut commands, game_resources.brick_purple.clone(), Vec3::new(x, purple_vertical_position, 0.), 100.);
    
            let red_vertical_position = purple_vertical_position + brick_vertical_space;
            brick_spawn_at(&mut commands, game_resources.brick_red.clone(), Vec3::new(x, red_vertical_position, 0.), 200.);
    
            x += brick_horizontal_space;
        }
    }
}

fn brick_spawn_at(commands: &mut Commands, texture: Handle<Image>, position: Vec3, points: f32) {
    commands.spawn(SpriteBundle {
        texture,
        transform: Transform {
            translation: position,
            ..Default::default()
        },
        ..Default::default()
    })
    .insert(Brick { points })
    .insert(SpriteSize::from(BRICK_SIZE_SPRITE));
}