use crate::{WinSize, GameTextures, PADDLE_SIZE_SPRITE, PlayerState, GameState};
use crate::components::{Velocity, Movable, Paddle, SpriteSize};

use bevy::ecs::system::Insert;
use bevy::prelude::*;

pub struct PaddlePlugin;

impl Plugin for PaddlePlugin {
	fn build(&self, app: &mut App) {
		app.add_startup_system_to_stage(StartupStage::PostStartup, spawn_paddle)
        .add_system(paddle_keyboard_event_system);
	}
}

fn spawn_paddle(mut commands: Commands, win_size: Res<WinSize>, game_resources: Res<GameTextures>) {
    commands.spawn(SpriteBundle {
        texture: game_resources.paddle.clone(),
        transform: Transform {
            translation: Vec3::new(0.,- win_size.h / 2. + PADDLE_SIZE_SPRITE.1 / 2. + 30., 0.),
            ..Default::default()
        },
        ..Default::default()
    })
    .insert(Paddle)
    .insert(Velocity {x: 0., y: 0.})
    .insert(Movable)
    .insert(SpriteSize::from(PADDLE_SIZE_SPRITE));
}

fn paddle_keyboard_event_system(
    game_state: Res<PlayerState>,
	kb: Res<Input<KeyCode>>,
    win_size: Res<WinSize>,
	mut query: Query<(&mut Velocity, &Transform), With<Paddle>>,
) {
	if let Ok((mut velocity , transform)) = query.get_single_mut() {
        if game_state.state != GameState::InGame { 
            velocity.x = 0.;
            velocity.y = 0.;
            return; 
        }
        
		velocity.x = if kb.pressed(KeyCode::Left) {
			-1.
		} else if kb.pressed(KeyCode::Right) {
			1.
		} else {
			0.
		};

        if transform.translation.x <= -win_size.w / 2. + PADDLE_SIZE_SPRITE.0 / 2. && velocity.x < 0. {
            velocity.x = 0.;
        }

        if transform.translation.x >= win_size.w / 2. - PADDLE_SIZE_SPRITE.0 / 2. && velocity.x > 0. {
            velocity.x = 0.;
        }
	}
}