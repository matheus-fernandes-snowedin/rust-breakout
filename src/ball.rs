use bevy::{prelude::*, sprite::collide_aabb::collide};

use crate::{WinSize, GameTextures, components::{Ball, Movable, Velocity, Paddle, SpriteSize, Brick}, BALL_SIZE_SPRITE, BRICK_SIZE_SPRITE, PlayerState, GameState};

pub struct BallPlugin;

impl Plugin for BallPlugin {
	fn build(&self, app: &mut App) {
		app
        .add_startup_system_to_stage(StartupStage::PostStartup, BallPlugin::internal_spawn_ball)
        .add_system(ball_movement)
        .add_system(ball_paddle_system)
        .add_system(ball_brick_system)
        .add_system(ball_start_keyboard_event_system);
	}
}

impl BallPlugin {
    fn internal_spawn_ball(mut commands: Commands, win_size: Res<WinSize>, game_resources: Res<GameTextures>) {
        BallPlugin::spawn_ball(&mut commands, &win_size, &game_resources);
    }

    pub fn spawn_ball(commands: &mut Commands, win_size: &Res<WinSize>, game_resources: &Res<GameTextures>) {
        commands.spawn(SpriteBundle {
            texture: game_resources.ball.clone(),
            transform: Transform {
                translation: Vec3::new(0., -win_size.h/2. + 125., 0.),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Ball::default())
        .insert(Movable)
        .insert(Velocity { x: 0., y: 0.})
        .insert(SpriteSize::from(BALL_SIZE_SPRITE));
    }
}

fn ball_movement(mut game_state: ResMut<PlayerState>, win_size: Res<WinSize>, mut query: Query<(&mut Velocity, &mut Transform), With<Ball>>) {
    for ((mut velocity, mut transform)) in query.iter_mut() {
        if transform.translation.x - BALL_SIZE_SPRITE.0 / 2. <= -win_size.w / 2. {
            velocity.x = -velocity.x;
            transform.translation.x = -win_size.w / 2. + BALL_SIZE_SPRITE.0 / 2.;
        }
        if transform.translation.x + BALL_SIZE_SPRITE.0 / 2. >= win_size.w / 2. {
            velocity.x = -velocity.x;
            transform.translation.x = win_size.w / 2. - BALL_SIZE_SPRITE.0 / 2.;
        }

        if transform.translation.y - BALL_SIZE_SPRITE.1 / 2. <= -win_size.h / 2. && game_state.state == GameState::InGame {
            game_state.state = GameState::GameOver;
        }
        if transform.translation.y + BALL_SIZE_SPRITE.0 / 2. >= win_size.h / 2. {
            velocity.y = -velocity.y;
            transform.translation.y = win_size.h / 2. - BALL_SIZE_SPRITE.1 / 2.;
        }
    }
}

fn ball_paddle_system(mut query_ball: Query<(&mut Velocity, &Transform, &SpriteSize), With<Ball>>, 
                      query_paddle: Query<(&Transform, &SpriteSize), With<Paddle>>
) {
    if let Ok((paddle_transform, paddle_size)) = query_paddle.get_single() {
        for ((mut velocity, ball_transform, ball_size)) in query_ball.iter_mut() {
            if ball_transform.translation.y < paddle_transform.translation.y {
                continue;
            }
            let collision = collide(
				paddle_transform.translation,
				paddle_size.0,
				ball_transform.translation,
				ball_size.0,
			);

			if let Some(_) = collision {
				// for now just invert the y direction
                velocity.y = velocity.y.abs();
			}
        }
    }
}

fn ball_brick_system(mut commands: Commands, 
                    mut game_state: ResMut<PlayerState>,
                    mut query_ball: Query<(&mut Velocity, &Transform, &SpriteSize), With<Ball>>, 
                    query_brick: Query<(Entity, &Transform, &SpriteSize, &Brick), With<Brick>>,
) {
    for ((mut ball_velocity, ball_transform, ball_size)) in query_ball.iter_mut() {
        for ((brick_entity, brick_transform, brick_size, brick)) in query_brick.iter() {
            let collision = collide(
                brick_transform.translation,
                brick_size.0,
                ball_transform.translation,
                ball_size.0,
            );
    
            if let Some(_) = collision {
                // revert ball speed depending on where we hit the brick
                commands.entity(brick_entity).despawn();
                game_state.points += brick.points;
                
                let mut y_distance = 0.;
                let mut x_distance = 0.;
                let mut x_velocity_signal = 1.;
                let mut y_velocity_signal = 1.;

                if ball_velocity.y < 0. {
                    y_distance = ((ball_transform.translation.y - BALL_SIZE_SPRITE.1) - (brick_transform.translation.y + BRICK_SIZE_SPRITE.1)).abs();
                    y_velocity_signal = 1.;
                } else {
                    y_distance = ((ball_transform.translation.y + BALL_SIZE_SPRITE.1) - (brick_transform.translation.y - BRICK_SIZE_SPRITE.1)).abs();
                    y_velocity_signal = -1.;
                }

                if ball_velocity.x < 0. {
                    x_distance = ((ball_transform.translation.x - BALL_SIZE_SPRITE.0) - (brick_transform.translation.x + BRICK_SIZE_SPRITE.0)).abs();
                    x_velocity_signal = 1.;
                } else {
                    x_distance = ((ball_transform.translation.x + BALL_SIZE_SPRITE.0) - (brick_transform.translation.x - BRICK_SIZE_SPRITE.0)).abs();
                    x_velocity_signal = -1.;
                }

                if y_distance < x_distance {
                    ball_velocity.y = ball_velocity.y.abs() * y_velocity_signal;
                } else {
                    ball_velocity.x = ball_velocity.x.abs() * x_velocity_signal;
                }
            }
        }
    }

    if query_brick.is_empty() && game_state.state == GameState::InGame {
        game_state.state = GameState::Victory;
    }
}

fn ball_start_keyboard_event_system(
	kb: Res<Input<KeyCode>>,
    mut game_state: ResMut<PlayerState>,
    win_size: Res<WinSize>,
	mut query_ball: Query<(&mut Velocity, &mut Ball), With<Ball>>, 
) {
    if game_state.state != GameState::WaitToStart { return; }
    if kb.pressed(KeyCode::Space) {
        for (mut ball_velocity, mut ball) in query_ball.iter_mut() {
            if ball.has_started() {continue;}
            ball.set_started(true);
            ball_velocity.x = 0.25;
            ball_velocity.y = 0.25;
            game_state.state = GameState::InGame;
        }
    }
}